package odeveloper.placekeeper.layout;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import odeveloper.placekeeper.R;
import odeveloper.placekeeper.Utils;
import odeveloper.placekeeper.adapter.PackageAdapter;
import odeveloper.placekeeper.db.PackageDto;

public class PackageActivity extends AppCompatActivity implements
        AdapterView.OnItemClickListener, View.OnClickListener{

    private PackageAdapter packageAdapter;
    private List<PackageDto> packageDtoList;

    private static final int PACKAGE_DETAILS_CODE = 1;
    private int IMAGE_ADD_DEFAULT = R.drawable.hotel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.package_list_layout);

        initPackageList();
        ListView packageListView = (ListView) findViewById(R.id.package_listview);
        packageAdapter = new PackageAdapter(this,packageDtoList);
        packageListView.setAdapter(packageAdapter);
        packageListView.setOnItemClickListener(this);

        FloatingActionButton newLocationBtn = (FloatingActionButton) findViewById(R.id.new_location_btn);
        newLocationBtn.setOnClickListener(this);
    }

    private void initPackageList() {
        packageDtoList = new ArrayList<PackageDto>();
        Cursor cursor = getContentResolver().query(PackageDto.Column.CONTENT_URI,null,null,null,null);
        if(Utils.isNotEmpty(cursor) && cursor.moveToFirst()){
            do {
                packageDtoList.add(new PackageDto(cursor));
            }while (cursor.moveToNext());
        }
//        Adding new package image
        PackageDto newPackage = new PackageDto(0,"",Utils.getDrawablePath(this,IMAGE_ADD_DEFAULT));
        packageDtoList.add(newPackage);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PackageDto packageDto = packageDtoList.get(position);
        if(packageDto != null){
            gotoDetails(packageDto.getId());
        }
    }

    private void gotoDetails(long packageId){
        Intent details = new Intent(this, PackageDetailsActivity.class);
        details.putExtra(Utils.EXTRA_DATA,packageId);
        startActivity(details);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.new_location_btn:
                Intent newLocation = new Intent(PackageActivity.this,DetectLocationActivity.class);
                startActivity(newLocation);
                break;
            default:
                break;
        }
    }
}
