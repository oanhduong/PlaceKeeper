package odeveloper.placekeeper.layout;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import odeveloper.placekeeper.DbUtils;
import odeveloper.placekeeper.R;
import odeveloper.placekeeper.Utils;
import odeveloper.placekeeper.db.PackageDto;

public class PackageDetailsActivity extends AppCompatActivity implements View.OnClickListener{

    private PackageDto packageDto;
    private final int DEFAULT_IMGAE = R.drawable.hotel;

    private EditText nameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.package_details_layout);

        long packageId = getIntent().getLongExtra(Utils.EXTRA_DATA,0);
        if(packageId == 0){
            packageDto = new PackageDto();
            packageDto.setImage(Utils.getDrawablePath(this,DEFAULT_IMGAE));
        }else{
            packageDto = DbUtils.getPackageById(this,packageId);
        }

        displayImage();
        displayName();
        displayButton();
    }

    private void displayButton() {
        Button action = (Button) findViewById(R.id.package_btn);
        action.setOnClickListener(this);
        if(packageDto.getId() == 0){
            action.setText(getString(R.string.category_create));
        }else{
            action.setText(getString(R.string.category_edit));
        }
    }

    private void displayName() {
        nameEditText = (EditText) findViewById(R.id.package_name);
        nameEditText.setText(packageDto.getName());
    }

    private void displayImage() {
        ImageView image = (ImageView) findViewById(R.id.package_image);
        if(Utils.isNotEmpty(packageDto.getImage())){
            image.setImageURI(Uri.parse(packageDto.getImage()));
        }
    }


    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Action clicked", Toast.LENGTH_SHORT).show();
    }
}
