package odeveloper.placekeeper.layout;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.vision.text.Text;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import odeveloper.placekeeper.background.PhotoTask;
import odeveloper.placekeeper.PlaceUtils;
import odeveloper.placekeeper.R;
import odeveloper.placekeeper.Utils;

public class PlaceDetailActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private GoogleApiClient mGoogleApiClient;
    private Place place;

    private ImageView photoImage;
    private TextView photoTitle;
    private TextView photoSubtitle;
    private TextView phoneNum;
    private TextView website;
    private AVLoadingIndicatorView avLoadingIndicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_layout);

        photoImage = (ImageView) findViewById(R.id.place_image);
        photoTitle = (TextView) findViewById(R.id.place_title);
        photoSubtitle = (TextView) findViewById(R.id.place_subtitle);
        phoneNum = (TextView) findViewById(R.id.place_phone_value);
        website = (TextView) findViewById(R.id.place_web_value);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        String placeId = getIntent().getStringExtra(Utils.EXTRA_DATA);
        getPlace(placeId);
    }

    private void displayPlace() {
        photoTitle.setText(PlaceUtils.getName(place));
        photoSubtitle.setText(PlaceUtils.getAddress(place));

        String phoneNumber = PlaceUtils.getPhoneNumber(place);
        phoneNum.setText(phoneNumber);
        phoneNum.setHint(phoneNumber);

        Uri websiteUri = PlaceUtils.getWebsite(place);
        if(websiteUri != null){
            website.setText(websiteUri.toString());
            website.setHint(websiteUri.toString());
        }
    }

    private void getPlace(String placeId){
        Places.GeoDataApi.getPlaceById(mGoogleApiClient,placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        if(places.getStatus().isSuccess() && places.getCount() > 0){
                            place = places.get(0).freeze();
                        }
                        places.release();
                        displayPlace();
                        placePhotoTask();
                    }
                });
    }

    private void placePhotoTask(){
        new PhotoTask(mGoogleApiClient){
            @Override
            protected void onPreExecute() {
                avLoadingIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.avi);
                avLoadingIndicatorView.show();
            }

            @Override
            protected void onPostExecute(List<AttributedPhoto> attributedPhotos) {
                if(attributedPhotos.isEmpty()){
                    return;
                }
                Bitmap photo = attributedPhotos.get(0).bitmap;
                photoImage.setImageBitmap(photo);
                avLoadingIndicatorView.hide();
            }

        }.execute(place.getId());
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
