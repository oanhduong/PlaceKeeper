package odeveloper.placekeeper.layout;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.wang.avi.AVLoadingIndicatorView;

import odeveloper.placekeeper.R;
import odeveloper.placekeeper.Utils;

public class DetectLocationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient placeClient;
    private static boolean isFirstTimeCreated;
    private AVLoadingIndicatorView avLoadingIndicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFirstTimeCreated = true;
        placeClient = new GoogleApiClient.Builder(DetectLocationActivity.this)
                .addConnectionCallbacks(DetectLocationActivity.this)
                .addOnConnectionFailedListener(DetectLocationActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    public void startPlacePicker(){
        PlacePicker.IntentBuilder placePickerBuilder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(placePickerBuilder.build(this),PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String placeId = null;
        if(requestCode == PLACE_PICKER_REQUEST){
            if(resultCode == RESULT_OK){
                Place place = PlacePicker.getPlace(this,data);
                placeId = place.getId();
            }
        }
        if(Utils.isNotEmpty(placeId)){
            Intent gotoDetails = new Intent(this, PlaceDetailActivity.class);
            gotoDetails.putExtra(Utils.EXTRA_DATA,placeId);
            startActivity(gotoDetails);
        }
        finish();
    }

    @Override
    protected void onStart() {
        placeClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        placeClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(isFirstTimeCreated){
            isFirstTimeCreated = false;
            new AsyncTask<String,Void,Void>(){
                @Override
                protected void onPreExecute() {
                    setContentView(R.layout.detect_location_layout);
                    avLoadingIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.avi);
                    avLoadingIndicatorView.show();
                }
                @Override
                protected Void doInBackground(String... params) {
                    startPlacePicker();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    avLoadingIndicatorView.show();
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /*public void getCurrentLocations(){
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(placeClient,null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {
                    if(placeLikelihoods.getCount() == 0){
                        return;
                    }
                    for(PlaceLikelihood place : placeLikelihoods){
                    }
                    placeLikelihoods.release();
                }
            });
        }
    }*/

    public void forTest(){
        //TODO Insert
        /*ContentValues values = new ContentValues();
        values.put(PackageDto.Column.NAME,"Hotel");
        values.put(PackageDto.Column.IMAGE,"/sdcard/abc.png");

        Uri uri = getContentResolver().insert(PackageDto.Column.CONTENT_URI,values);
        Toast.makeText(this, uri.toString(), Toast.LENGTH_SHORT).show();*/

        /*ContentValues values = new ContentValues();
        values.put(PlaceDto.Column.NAME,"Cafe Coi Rieng");
        values.put(PlaceDto.Column.LAT,"1234");
        values.put(PlaceDto.Column.LON,"2344");
        values.put(PlaceDto.Column.IMAGES,"sdcard/abc.png");
        values.put(PlaceDto.Column.NOTE,"nhan vien kho chiu");
        values.put(PlaceDto.Column.PACKAGE_ID,3);
        Uri uri = getContentResolver().insert(PlaceDto.Column.CONTENT_URI,values);
        Toast.makeText(this, uri.toString(), Toast.LENGTH_SHORT).show();*/

        //TODO Select
        /*Cursor cursor = getContentResolver().query(PackageDto.Column.CONTENT_URI,null,null,null,null);
        if(Utils.isNotEmpty(cursor)){
            if(cursor.moveToFirst()){
                do {
                    Toast.makeText(this, "" + cursor.getString(cursor.getColumnIndex(PackageDto.Column.NAME)), Toast.LENGTH_SHORT).show();
                }while (cursor.moveToNext());
            }
        }*/

        /*Cursor cursor = getContentResolver().query(PlaceDto.Column.CONTENT_URI,null,null,null,null);
        if(Utils.isNotEmpty(cursor)){
            if(cursor.moveToFirst()){
                do {
                    Toast.makeText(this, "" +
                            cursor.getString(cursor.getColumnIndex(PlaceDto.Column.NAME))  +
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.LON)) +
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.LAT)) +
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.PACKAGE_ID)),Toast.LENGTH_SHORT).show();
                }while (cursor.moveToNext());
            }
        }*/

        /*Uri searchWithId = ContentUris.withAppendedId(PlaceDto.Column.CONTENT_URI,2);
        Cursor cursor = getContentResolver().query(searchWithId,null,null,null,null);
        if(Utils.isNotEmpty(cursor)){
            if(cursor.moveToFirst()){
                do {
                    Toast.makeText(this, "" +
                            cursor.getString(cursor.getColumnIndex(PlaceDto.Column.NAME))  +
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.LON)) +
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.LAT)) +
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.PACKAGE_ID)),Toast.LENGTH_SHORT).show();
                }while (cursor.moveToNext());
            }
        }*/

        /*Cursor cursor = getContentResolver().query(PlaceDto.Column.CONTENT_URI,
                null,
                PlaceDto.Column.WHERE_PACKAGE_ID,new String[]{"5"},null);
        if(Utils.isNotEmpty(cursor)){
            if(cursor.moveToFirst()){
                do {
                    Toast.makeText(this, "" +
                            cursor.getString(cursor.getColumnIndex(PlaceDto.Column.NAME))  +
                            " "+
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.LON)) +
                            " "+
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.LAT)) +
                            " "+
                            cursor.getInt(cursor.getColumnIndex(PlaceDto.Column.PACKAGE_ID)),Toast.LENGTH_SHORT).show();
                }while (cursor.moveToNext());
            }
        }*/

        //TODO Update
        /*ContentValues values = new ContentValues();
        values.put(PackageDto.Column.NAME,"Cafe");
        values.put(PackageDto.Column.IMAGE,"/sdcard/abc.png");

        int i = getContentResolver().update(ContentUris.withAppendedId(PackageDto.Column.CONTENT_URI,1),values,null,null);
        Toast.makeText(this, String.valueOf(i), Toast.LENGTH_SHORT).show();*/

        /*ContentValues values = new ContentValues();
        values.put(PlaceDto.Column.NAME,"Cafe Soi Da");
        values.put(PlaceDto.Column.LAT,"1234");
        values.put(PlaceDto.Column.LON,"2344");
        values.put(PlaceDto.Column.IMAGES,"sdcard/abc.png");
        values.put(PlaceDto.Column.NOTE,"nhan vien kho chiu");
        values.put(PlaceDto.Column.PACKAGE_ID,2);
        int i = getContentResolver().update(ContentUris.withAppendedId(PlaceDto.Column.CONTENT_URI,1),values,null,null);
        Toast.makeText(this, String.valueOf(i), Toast.LENGTH_SHORT).show();*/

        //TODO CountAll
        /*Cursor cursor = getContentResolver().query(PlaceDto.Column.CONTENT_URI,
                new String[]{PlaceDto.Column._ID},
                PlaceDto.Column.WHERE_PACKAGE_ID,
                new String[]{String.valueOf(3)},
                null);
        if(cursor != null){
            Toast.makeText(this, ""+cursor.getCount(), Toast.LENGTH_SHORT).show();
        }*/

        //TODO Delete
        /*Uri delete = ContentUris.withAppendedId(PlaceDto.Column.CONTENT_URI,1);
        int i = getContentResolver().delete(delete,null,null);
        Toast.makeText(this, ""+i, Toast.LENGTH_SHORT).show();*/

        /*Uri delete = ContentUris.withAppendedId(PackageDto.Column.CONTENT_URI,3);
        int i = getContentResolver().delete(delete,null,null);
        Toast.makeText(this, ""+i, Toast.LENGTH_SHORT).show();*/
    }
}
