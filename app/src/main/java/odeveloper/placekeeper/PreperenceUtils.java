package odeveloper.placekeeper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by oanh.duong on 10/17/16.
 */

public class PreperenceUtils {

    public static final String FIRST_TIME = "first_time";

    public static boolean isFirstTime(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean firstTime = preferences.getBoolean(FIRST_TIME,true);
        return firstTime;
    }
}
