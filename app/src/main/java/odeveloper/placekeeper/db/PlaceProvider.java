package odeveloper.placekeeper.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import odeveloper.placekeeper.Utils;

/**
 * Created by oanh.duong on 10/12/16.
 */

public class PlaceProvider extends ContentProvider {

    private SQLiteOpenHelper sqLiteOpenHelper;

    private static final int PACKAGES = 1;
    private static final int PACKAGE_ID = 2;
    private static final int PLACES = 3;
    private static final int PLACE_ID = 4;

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(Utils.AUTHORITY, PackageDto.Column.PATH, PACKAGES);
        URI_MATCHER.addURI(Utils.AUTHORITY, PackageDto.Column.PATH + "/#", PACKAGE_ID);
        URI_MATCHER.addURI(Utils.AUTHORITY, PlaceDto.Column.PATH, PLACES);
        URI_MATCHER.addURI(Utils.AUTHORITY, PlaceDto.Column.PATH + "/#", PLACE_ID);
    }

    private static final class DbHelper extends SQLiteOpenHelper{
        private static final String DB_NAME = "placekeeper.db";
        private static final int DB_VERSION = 1;

        public static boolean isDbCreated = false;

        private static final String CREATE_TABLE_PACKAGE =
                String.format("create table %s(" +
                        "%s integer primary key," +
                        "%s text," +
                        "%s text);",
                        PackageDto.Column.TABLE_NAME,
                        PackageDto.Column._ID,
                        PackageDto.Column.NAME,
                        PackageDto.Column.IMAGE);

        private static final String CREATE_TABLE_PLACE =
                String.format("create table %s(" +
                                "%s integer primary key," +
                                "%s text," +
                                "%s text," +
                                "%s text," +
                                "%s text," +
                                "%s integer);",
                        PlaceDto.Column.TABLE_NAME,
                        PlaceDto.Column._ID,
                        PlaceDto.Column.NAME,
                        PlaceDto.Column.PLACE_GOOGLE_ID,
                        PlaceDto.Column.NOTE,
                        PlaceDto.Column.IMAGES,
                        PlaceDto.Column.PACKAGE_ID);

        private static final String DROP_TABLE_PACKAGE =
                String.format("drop table if exists %s",PackageDto.Column.TABLE_NAME);

        private static final String DROP_TABLE_PLACE =
                String.format("drop table if exists %s",PlaceDto.Column.TABLE_NAME);

        public DbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_PACKAGE);
            db.execSQL(CREATE_TABLE_PLACE);
            isDbCreated = true;
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public void addSamplePackages() {
        Cursor cursor = query(PackageDto.Column.CONTENT_URI,
                new String[]{PackageDto.Column._ID},
                null,null,null);
        if(Utils.isNotEmpty(cursor)){
            return;
        }
        int length = PackageDto.SAMPLE_PACKAGES.length;
        if(length > 0 && length % 2 == 0){ //Sample package must be a list of couple
            for(int idx = 0; idx < length; idx += 2){
                String name = getContext().getResources().getString(PackageDto.SAMPLE_PACKAGES[idx]);
                String image = Utils.getDrawablePath(getContext(),PackageDto.SAMPLE_PACKAGES[idx+1]);

                ContentValues values = new ContentValues();
                values.put(PackageDto.Column.NAME,name);
                values.put(PackageDto.Column.IMAGE,image);
                insert(PackageDto.Column.CONTENT_URI, values);
            }
        }
    }

    @Override
    public boolean onCreate() {
        sqLiteOpenHelper = new DbHelper(getContext());
        addSamplePackages();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)){
            case PACKAGES:
                sqLiteQueryBuilder.setTables(PackageDto.Column.TABLE_NAME);
                break;
            case PACKAGE_ID:
                sqLiteQueryBuilder.setTables(PackageDto.Column.TABLE_NAME);
                sqLiteQueryBuilder.appendWhere(PackageDto.Column._ID+"=");
                sqLiteQueryBuilder.appendWhere(uri.getPathSegments().get(1));
                break;
            case PLACES:
                sqLiteQueryBuilder.setTables(PlaceDto.Column.TABLE_NAME);
                break;
            case PLACE_ID:
                sqLiteQueryBuilder.setTables(PlaceDto.Column.TABLE_NAME);
                sqLiteQueryBuilder.appendWhere(PlaceDto.Column._ID+"=");
                sqLiteQueryBuilder.appendWhere(uri.getPathSegments().get(1));
                break;
            default:
                return null;
        }

        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        Cursor result = sqLiteQueryBuilder.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);

        if(Utils.isNotEmpty(result)){
            result.setNotificationUri(getContext().getContentResolver(),uri);
            return result;
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if(values == null){
            return null;
        }
        SQLiteDatabase writeDb = sqLiteOpenHelper.getWritableDatabase();
        long rowId = 0;
        Uri newUri = null;
        switch (URI_MATCHER.match(uri)){
            case PACKAGES:
                if(PackageDto.isValid(values)){
                    rowId = writeDb.insert(PackageDto.Column.TABLE_NAME,null,values);
                    newUri = ContentUris.withAppendedId(PackageDto.Column.CONTENT_URI,rowId);
                }
                break;
            case PLACES:
                if(PlaceDto.isValid(values)){
                    Uri getPackage = ContentUris.withAppendedId(PackageDto.Column.CONTENT_URI, values.getAsLong(PlaceDto.Column.PACKAGE_ID));
                    Cursor packageCursor = query(getPackage,new String[]{PackageDto.Column._ID},null,null,null);
                    if(packageCursor != null && packageCursor.getCount() == 1){
                        rowId = writeDb.insert(PlaceDto.Column.TABLE_NAME,null,values);
                        newUri = ContentUris.withAppendedId(PlaceDto.Column.CONTENT_URI,rowId);
                    }
                }
                break;
            default:
                return null;
        }
        if(rowId > 0){
            getContext().getContentResolver().notifyChange(newUri,null);
            return newUri;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();
        int count = 0;
        long rowId;
        String segment;
        switch (URI_MATCHER.match(uri)) {
            case PACKAGES:
                count = db.delete(PackageDto.Column.TABLE_NAME, selection, selectionArgs);
                break;
            case PACKAGE_ID:
                segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);

                Cursor cursor = query(PlaceDto.Column.CONTENT_URI,
                        new String[]{PlaceDto.Column._ID},
                        PlaceDto.Column.WHERE_PACKAGE_ID,
                        new String[]{String.valueOf(rowId)},
                        null);
                if(Utils.isNotEmpty(cursor)){
                    //If package contains places, don't allow to delete it
                    //You must delete all places in package first
                    break;
                }

                if (TextUtils.isEmpty(selection)) {
                    selection = PackageDto.Column._ID + "=" + rowId;
                } else {
                    selection = PackageDto.Column._ID + "=" + rowId + " AND (" + selection + ")";
                }
                count = db.delete(PackageDto.Column.TABLE_NAME, selection, selectionArgs);
                break;
            case PLACES:
                count = db.delete(PlaceDto.Column.TABLE_NAME, selection, selectionArgs);
                break;
            case PLACE_ID:
                segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);
                if (TextUtils.isEmpty(selection)) {
                    selection = PlaceDto.Column._ID + "=" + rowId;
                } else {
                    selection = PlaceDto.Column._ID + "=" + rowId + " AND (" + selection + ")";
                }
                count = db.delete(PackageDto.Column.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                break;
        }
        if(count > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;
        long rowId;
        String segment;
        SQLiteDatabase writeDb = sqLiteOpenHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case PACKAGE_ID: {
                segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);
                if(PackageDto.isValid(values)){
                    count = writeDb.update(PackageDto.Column.TABLE_NAME, values,
                            PackageDto.Column._ID + "=" + rowId, null);
                }
                break;
            }
            case PLACE_ID: {
                segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);
                if(PlaceDto.isValid(values)) {
                    Uri getPackage = ContentUris.withAppendedId(PackageDto.Column.CONTENT_URI, values.getAsLong(PlaceDto.Column.PACKAGE_ID));
                    Cursor packageCursor = query(getPackage, new String[]{PackageDto.Column._ID}, null, null, null);
                    if (Utils.isNotEmpty(packageCursor)) {
                        count = writeDb.update(PlaceDto.Column.TABLE_NAME, values,
                                PlaceDto.Column._ID + "=" + rowId, null);
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
        if(count > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

}
