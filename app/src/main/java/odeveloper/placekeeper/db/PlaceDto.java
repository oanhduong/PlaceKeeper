package odeveloper.placekeeper.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import odeveloper.placekeeper.Utils;

/**
 * Created by oanh.duong on 10/12/16.
 */

public class PlaceDto implements Parcelable {
    private long id;
    private String name;
    private String placeGoogleId;
    private String note;
    private String images; //split by ;
    private long packageId;

    public PlaceDto(Cursor cursor){
        id = cursor.getLong(cursor.getColumnIndex(Column._ID));
        name = cursor.getString(cursor.getColumnIndex(Column.NAME));
        placeGoogleId = cursor.getString(cursor.getColumnIndex(Column.PLACE_GOOGLE_ID));
        note = cursor.getString(cursor.getColumnIndex(Column.NOTE));
        images = cursor.getString(cursor.getColumnIndex(Column.IMAGES));
        packageId = cursor.getLong(cursor.getColumnIndex(Column.PACKAGE_ID));
    }

    public static boolean isValid(ContentValues values){
        String plName = values.getAsString(Column.NAME);
        String placeGoogleId = values.getAsString(Column.PLACE_GOOGLE_ID);
        long pkId = values.getAsLong(Column.PACKAGE_ID);
        return Utils.isNotEmpty(plName) && Utils.isNotEmpty(placeGoogleId) && pkId > 0;
    }
    public static class Column implements BaseColumns{
        public static final String PATH = "placedto";
        public static final Uri CONTENT_URI =
                Uri.parse("content://"+ Utils.AUTHORITY+"/"+PATH);

        public static final String TABLE_NAME = "places";
        public static final String NAME = "name";
        public static final String PLACE_GOOGLE_ID = "placeGoogleId";
        public static final String NOTE = "note";
        public static final String IMAGES = "images";
        public static final String PACKAGE_ID = "packageId";

        public static final String WHERE_PACKAGE_ID = PACKAGE_ID + " = ? ";

    }

    public PlaceDto(long id, String name, String placeGoogleId, String note, String images, long packageId) {
        this.id = id;
        this.name = name;
        this.placeGoogleId = placeGoogleId;
        this.note = note;
        this.images = images;
        this.packageId = packageId;
    }

    protected PlaceDto(Parcel in) {
        id = in.readLong();
        name = in.readString();
        placeGoogleId = in.readString();
        note = in.readString();
        images = in.readString();
        packageId = in.readLong();
    }

    public static final Creator<PlaceDto> CREATOR = new Creator<PlaceDto>() {
        @Override
        public PlaceDto createFromParcel(Parcel in) {
            return new PlaceDto(in);
        }

        @Override
        public PlaceDto[] newArray(int size) {
            return new PlaceDto[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceGoogleId(){
        return placeGoogleId;
    }

    public void setPlaceGoogleId(String placeGoogleId){
        this.placeGoogleId = placeGoogleId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public long getPackageId() {
        return packageId;
    }

    public void setPackageId(long packageId) {
        this.packageId = packageId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(placeGoogleId);
        dest.writeString(note);
        dest.writeString(images);
        dest.writeLong(packageId);
    }
}
