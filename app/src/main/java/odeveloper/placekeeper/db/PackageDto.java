package odeveloper.placekeeper.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import odeveloper.placekeeper.R;
import odeveloper.placekeeper.Utils;

/**
 * Created by oanh.duong on 10/12/16.
 */

public class PackageDto implements Parcelable{
    private long id;
    private String name;
    private String image;

    public static int[] SAMPLE_PACKAGES = new int []{ //Contain name resource id, image resource id
            R.string.personal_house,R.drawable.hotel,
            R.string.eating_drinking,R.drawable.hotel,
            R.string.hotel,R.drawable.hotel,
            R.string.shopping,R.drawable.hotel,
            R.string.entertainment,R.drawable.hotel,
            R.string.medical,R.drawable.hotel,
            R.string.others,R.drawable.hotel
    };

    public PackageDto(){}

    public PackageDto(Cursor cursor){
        id = cursor.getLong(cursor.getColumnIndex(Column._ID));
        name = cursor.getString(cursor.getColumnIndex(Column.NAME));
        image = cursor.getString(cursor.getColumnIndex(Column.IMAGE));
    }

    public static boolean isValid(ContentValues contentValues){
        String pkName = contentValues.getAsString(Column.NAME);
        return Utils.isNotEmpty(pkName);
    }

    public static class Column implements BaseColumns {
        public static final String PATH = "packagedto";
        public static final Uri CONTENT_URI =
                Uri.parse("content://"+ Utils.AUTHORITY+"/"+PATH);

        public static final String TABLE_NAME = "packages";
        public static final String NAME = "name";
        public static final String IMAGE = "image";

    }

    public PackageDto(long id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public PackageDto(String name, String image) {
        this.name = name;
        this.image = image;
    }

    protected PackageDto(Parcel in) {
        id = in.readLong();
        name = in.readString();
        image = in.readString();
    }

    public static final Creator<PackageDto> CREATOR = new Creator<PackageDto>() {
        @Override
        public PackageDto createFromParcel(Parcel in) {
            return new PackageDto(in);
        }

        @Override
        public PackageDto[] newArray(int size) {
            return new PackageDto[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(image);
    }
}
