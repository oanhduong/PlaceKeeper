package odeveloper.placekeeper;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import odeveloper.placekeeper.db.PackageDto;
import odeveloper.placekeeper.db.PlaceDto;

/**
 * Created by oanh.duong on 10/19/16.
 */

public class DbUtils {

    public static int countPlaceInPackage(Context mContext, long packageId){
        Cursor countPlaces = mContext.getContentResolver().query(PlaceDto.Column.CONTENT_URI,
                new String[]{PlaceDto.Column._ID},
                PlaceDto.Column.WHERE_PACKAGE_ID,
                new String[]{String.valueOf(packageId)},
                null);
        return countPlaces != null ? countPlaces.getCount() : 0;
    }

    public static PackageDto getPackageById(Context mContext, long packageId){
        Uri query = ContentUris.withAppendedId(PackageDto.Column.CONTENT_URI,packageId);
        Cursor getItem = mContext.getContentResolver().query(query,null,null,null,null);
        if(Utils.isNotEmpty(getItem) && getItem.moveToFirst()){
            return new PackageDto(getItem);
        }
        return null;
    }
}
