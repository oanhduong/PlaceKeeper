package odeveloper.placekeeper.background;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oanh.duong on 10/14/16.
 */

public abstract class PhotoTask extends AsyncTask<String,Void,List<PhotoTask.AttributedPhoto>> {

    private GoogleApiClient mGoogleApiClient;

    public PhotoTask(GoogleApiClient googleApiClient){
        mGoogleApiClient = googleApiClient;
    }

    @Override
    protected List<AttributedPhoto> doInBackground(String... params) {
        if(params.length < 1){
            return null;
        }
        final String placeId = params[0];
        List<AttributedPhoto> photos = new ArrayList<AttributedPhoto>();

        PlacePhotoMetadataResult result = Places.GeoDataApi.getPlacePhotos(mGoogleApiClient,placeId).await();
        if(result.getStatus().isSuccess()){
            PlacePhotoMetadataBuffer photoMetadataBuffer = result.getPhotoMetadata();
            if(photoMetadataBuffer.getCount() > 0 && !isCancelled()){
                for(PlacePhotoMetadata photo : photoMetadataBuffer){
                    CharSequence photoAttr = photo.getAttributions();
                    Bitmap image = photo.getPhoto(mGoogleApiClient).await().getBitmap();
                    AttributedPhoto attributedPhoto = new AttributedPhoto(photoAttr,image);
                    photos.add(attributedPhoto);
                }
                photoMetadataBuffer.release();
            }
        }
        return photos;
    }

    public class AttributedPhoto {
        public final CharSequence attribution;
        public final Bitmap bitmap;


        public AttributedPhoto(CharSequence attribution, Bitmap bitmap) {
            this.attribution = attribution;
            this.bitmap = bitmap;
        }
    }
}
