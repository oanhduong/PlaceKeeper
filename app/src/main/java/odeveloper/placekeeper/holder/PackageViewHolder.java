package odeveloper.placekeeper.holder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by oanh.duong on 10/15/16.
 */

public class PackageViewHolder {
    public ImageView image;
    public TextView title;
    public TextView subtitle;
    public TextView addTitle;
    public ImageView actionImage;

    public PackageViewHolder(ImageView container, TextView title, TextView subtitle, TextView addTitle, ImageView actionImage){
        this.image = container;
        this.title = title;
        this.subtitle = subtitle;
        this.addTitle = addTitle;
        this.actionImage = actionImage;
    }

    public PackageViewHolder(){

    }
}
