package odeveloper.placekeeper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import odeveloper.placekeeper.DbUtils;
import odeveloper.placekeeper.R;
import odeveloper.placekeeper.Utils;
import odeveloper.placekeeper.db.PackageDto;
import odeveloper.placekeeper.holder.PackageViewHolder;
import odeveloper.placekeeper.layout.PackageDetailsActivity;

/**
 * Created by oanh.duong on 10/15/16.
 */

public class PackageAdapter extends BaseAdapter implements PopupMenu.OnMenuItemClickListener {

    private List<PackageDto> mPackageDtoList;
    private Context mContext;
    private int currentPos;

    public PackageAdapter(Context context, List<PackageDto> packageDtos){
        mPackageDtoList = packageDtos;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mPackageDtoList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPackageDtoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((PackageDto) getItem(position)).getId();
    }

    public int getCurrentPos(){return currentPos;}

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PackageViewHolder mPackageViewHolder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        PackageDto packageDto = (PackageDto) getItem(position);
        long packageId = packageDto.getId();

        if(convertView == null){
            convertView = inflater.inflate(R.layout.package_item_layout,parent,false);
            mPackageViewHolder = new PackageViewHolder(
                    (ImageView) convertView.findViewById(R.id.package_item_image),
                    (TextView) convertView.findViewById(R.id.package_item_title),
                    (TextView) convertView.findViewById(R.id.package_item_subtitle),
                    (TextView) convertView.findViewById(R.id.package_item_add_title),
                    (ImageView) convertView.findViewById(R.id.package_item_action)
            );
            convertView.setTag(mPackageViewHolder);
        }else{
            mPackageViewHolder = (PackageViewHolder) convertView.getTag();
        }

        mPackageViewHolder.image.setImageURI(Uri.parse(packageDto.getImage()));

        if(packageId > 0){
            mPackageViewHolder.title.setVisibility(View.VISIBLE);
            mPackageViewHolder.title.setText(packageDto.getName());

            mPackageViewHolder.subtitle.setVisibility(View.VISIBLE);
            mPackageViewHolder.subtitle.setText(mContext.getString(R.string.places_count,
                    DbUtils.countPlaceInPackage(mContext,packageId)));

            mPackageViewHolder.actionImage.setVisibility(View.VISIBLE);
            mPackageViewHolder.actionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPos = position;
                    showMenuPopup(v);
                }
            });
            mPackageViewHolder.addTitle.setVisibility(View.GONE);
        }else {
            mPackageViewHolder.title.setVisibility(View.GONE);
            mPackageViewHolder.subtitle.setVisibility(View.GONE);
            mPackageViewHolder.actionImage.setVisibility(View.GONE);

            mPackageViewHolder.addTitle.setVisibility(View.VISIBLE);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Add new package", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return convertView;
    }

    private void showMenuPopup(View v){
        PopupMenu popupMenu = new PopupMenu(mContext,v);
        popupMenu.setOnMenuItemClickListener(this);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.package_item_menu,popupMenu.getMenu());
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.map_action:
                return true;
            case R.id.edit_action:
                PackageDto packageDto = (PackageDto) getItem(currentPos);
                if(packageDto != null){
                    gotoDetails(packageDto.getId());
                }
                return true;
            case R.id.delete_action:
                return true;
            default:
                return false;
        }
    }

    private void gotoDetails(long packageId){
        Intent details = new Intent(mContext, PackageDetailsActivity.class);
        details.putExtra(Utils.EXTRA_DATA,packageId);
        mContext.startActivity(details);
    }
}
