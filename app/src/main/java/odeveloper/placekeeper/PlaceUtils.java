package odeveloper.placekeeper;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.location.places.Place;

/**
 * Created by oanh.duong on 10/13/16.
 */

public class PlaceUtils {
    public static String displayPlace(@NonNull Place place) {
        String content = "";
        content += getName(place) + " " +
                getAddress(place) + " " +
                getPhoneNumber(place) + " " +
                getWebsite(place);
        return content;
    }

    public static String getName(@NonNull Place place){
        String name = place.getName().toString();
        if(Utils.isNotEmpty(name)){
            return name;
        }
        return "";
    }

    public static final String getAddress(@NonNull Place place){
        String address = place.getAddress().toString();
        if(Utils.isNotEmpty(address)){
            return address;
        }
        return "";
    }

    public static final String getPhoneNumber(@NonNull Place place){
        String phoneNum = place.getPhoneNumber().toString();
        if(Utils.isNotEmpty(phoneNum)){
            return phoneNum;
        }
        return "";
    }

    public static final Uri getWebsite(@NonNull Place place){
        return place.getWebsiteUri();
    }

}
