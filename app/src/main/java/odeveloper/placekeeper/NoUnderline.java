package odeveloper.placekeeper;

import android.annotation.SuppressLint;
import android.text.TextPaint;
import android.text.style.URLSpan;

/**
 * Created by oanh.duong on 10/16/16.
 */

@SuppressLint("ParcelCreator")
public class NoUnderline extends URLSpan {

    public NoUnderline(String url) {
        super(url);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }
}
