package odeveloper.placekeeper;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Spannable;
import android.text.style.URLSpan;

import java.io.File;
import java.util.Locale;

/**
 * Created by oanh.duong on 10/13/16.
 */

public class Utils {
    public static final String AUTHORITY = "odeveloper.placekeeper";
    public static final String EXTRA_DATA = "data";
    public static final boolean IS_VIETNAMESE_LANG = Locale.getDefault().getLanguage().equals("vi");

    public static boolean isNotEmpty(Object object){
        if(object == null){
            return false;
        }
        if(object instanceof  String){
            return ((String) object).isEmpty() == false;
        }

        if(object instanceof Cursor){
            return ((Cursor) object).getCount() > 0;
        }

        return false;
    }

    public static Spannable removeUnderline(Spannable text){
        URLSpan[] spans = text.getSpans(0,text.length(),URLSpan.class);
        for(URLSpan span : spans){
            int start = text.getSpanStart(span);
            int end = text.getSpanEnd(span);
            text.removeSpan(span);
            span = new NoUnderline(span.getURL());
            text.setSpan(span,start,end,0);
        }
        return text;
    }

    public static String getDrawablePath(Context context, int drawableId){
        Uri path = Uri.parse("android.resource://" +
                context.getPackageName() +
                "/" +
                drawableId);
        return path.toString();
    }

    public static Bitmap getBitmapFromPath(String path){
        File file = new File(path);
        if(file.exists()){
            return BitmapFactory.decodeFile(file.getAbsolutePath());
        }
        return null;
    }

}
